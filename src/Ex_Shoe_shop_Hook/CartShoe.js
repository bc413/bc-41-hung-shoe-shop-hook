import React from "react";

export default function CartShoe({ cart, handleUpdateSL, handleDelete }) {
  let renderCartShoe = () => {
    return cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img style={{ width: "100px" }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                handleUpdateSL(item.id, 1);
              }}
              className="btn btn-primary"
            >
              +
            </button>
            <strong>{item.soLuong}</strong>
            <button
              onClick={() => {
                handleUpdateSL(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                handleDelete(item.id);
              }}
              class="btn btn-danger"
            >
              DELETE
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <p>CartShoe</p>
      <table className="w-100">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Số Lượng</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{renderCartShoe()}</tbody>
      </table>
    </div>
  );
}
