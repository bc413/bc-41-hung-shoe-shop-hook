import React from "react";
import ItemShoe from "./ItemShoe";

export default function ListShoe({ arrShoeShop, handleAddToCart }) {
  let renderShoe = () => {
    return arrShoeShop.map((item, index) => {
      return <ItemShoe item={item} key={index} clickCart={handleAddToCart} />;
    });
  };

  return (
    <div>
      <p>List Shoe</p>
      <div className="row">{renderShoe()}</div>
    </div>
  );
}
