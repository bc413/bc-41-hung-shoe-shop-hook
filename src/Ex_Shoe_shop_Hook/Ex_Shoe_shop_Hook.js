import React, { useState } from "react";
import CartShoe from "./CartShoe";
import { dataShoeShop } from "./Data_Shoe";
import ListShoe from "./ListShoe";

export default function Ex_Shoe_shop_Hook() {
  let [state, setState] = useState({
    dataShoe: dataShoeShop,
  });

  let [cart, setCart] = useState({
    cart: [],
  });

  let handleAddToCart = (shoe) => {
    let newCart = [...cart.cart];
    let newObjec = { ...shoe, soLuong: 1 };
    let index = newCart.findIndex((item) => {
      return item.id === shoe.id;
    });

    if (index == -1) {
      newCart.push(newObjec);
    } else {
      newCart[index].soLuong++;
    }
    setCart({
      cart: newCart,
    });
  };
  let handleUpdateSL = (shoe, number) => {
    let newCart = [...cart.cart];
    console.log(shoe);
    let index = newCart.findIndex((item) => {
      return item.id === shoe;
    });
    console.log(index);
    if (index != -1) {
      newCart[index].soLuong += number;
    }
    if (newCart[index].soLuong === 0) {
      newCart.splice(index, 1);
    }
    setCart({
      cart: newCart,
    });
  };
  let handleDelete = (idShoe) => {
    let newCart = [...cart.cart];
    let index = newCart.findIndex((item) => {
      return item.id === idShoe;
    });
    if (index != -1) {
      newCart.splice(index, 1);
    }
    setCart({
      cart: newCart,
    });
  };

  return (
    <div className="row">
      <div className="col-6">
        <CartShoe
          cart={cart.cart}
          handleUpdateSL={handleUpdateSL}
          handleDelete={handleDelete}
        />
      </div>
      <div className="col-6">
        <ListShoe
          arrShoeShop={state.dataShoe}
          handleAddToCart={handleAddToCart}
        />
      </div>
    </div>
  );
}
