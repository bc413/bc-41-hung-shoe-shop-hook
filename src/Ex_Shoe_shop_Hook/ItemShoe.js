import React from "react";
import { Card } from "antd";
const { Meta } = Card;

export default function ItemShoe({ item, clickCart }) {
  return (
    <div className="col-4">
      <Card
        hoverable
        style={{
          width: 240,
        }}
        cover={<img alt="example" src={item.image} />}
      >
        <Meta title={item.name} />
        <p>{item.price}$</p>
        <button
          onClick={() => {
            clickCart(item);
          }}
          className="btn btn-primary"
        >
          Add
        </button>
      </Card>
    </div>
  );
}
