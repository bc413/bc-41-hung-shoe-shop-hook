import logo from "./logo.svg";
import "./App.css";
import Ex_Shoe_shop_Hook from "./Ex_Shoe_shop_Hook/Ex_Shoe_shop_Hook";

function App() {
  return (
    <div className="App">
      <Ex_Shoe_shop_Hook />
    </div>
  );
}

export default App;
